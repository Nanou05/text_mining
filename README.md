

# Execution Instructions

For the best experience, it is highly recommended to run this code in Google Colab, accessible via the link provided below. Google Colab offers a clean, isolated environment ensuring consistent performance across different setups, and eliminates potential issues with package dependencies and system configurations.
The link to acces the code : " https://colab.research.google.com/drive/1pD1y3GPlyruaqJVdmjN-qZsqOs0mIMO0?usp=sharing "

# PubMed Study Scraper, Preprocessor, and Translator

This project contains Python scripts to scrape study data from PubMed, preprocess the scraped data, and translate the study abstracts.

## Setup

1. Clone this repository to your local machine.
2. Ensure you have Python 3.8 or newer installed.
3. Install the required Python libraries using pip:

```
pip install -r requirements.txt
```

## Usage

1. To scrape study data from PubMed, run the scraper script with the chunk size as an optional argument (default is 100):

```
python scraper.py --chunk_size 10
```

This will create a CSV file named `pubmed_disease_studies.csv`.

2. Preprocess the scraped data using the preprocessor script:

```
python preprocessor.py
```

This will filter and clean the abstracts in the CSV file and save the result as `pubmed_disease_studies_filtered.csv`.

3. Translate the study abstracts using the translator script. You can specify the number of lines to translate as an optional argument (if not specified, all lines will be translated):

```
python translator.py --lines 10
```

This will translate the abstracts to French and Spanish and save the result as `pubmed_disease_studies_translated.csv`.




