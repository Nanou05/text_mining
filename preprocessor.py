
# Standard library imports
import re
import nltk
# Third-party imports
import pandas as pd
from nltk import download
from tqdm import tqdm
from spacy.lang.en.stop_words import STOP_WORDS



# Download necessary resources
download('stopwords')  # NLTK's stopwords
nltk.download('punkt')  # Download the necessary resources


# PREPROCESSING-----------------------------------------------------------------------------------------------------------------------------------------

def preprocess_text(text):
    """
    Preprocess the text: Lowercase, remove non-alphabetic characters, sentence segmentation, and remove extra spaces.
    """
    # Lowercase the text
    text = text.lower()

    # Remove non-alphabetic characters except punctuation marks
    text = re.sub('[^a-z\s.,?!]+', ' ', text.lower())

    # Sentence Segmentation
    sentences = nltk.sent_tokenize(text)

    # Remove extra spaces
    sentences = [re.sub('\s+', ' ', sentence).strip() for sentence in sentences]

    return ' '.join(sentences)

def load_and_filter_data(file_path):
    """
    Load the CSV into a pandas DataFrame and filter it to keep only rows with non-empty abstracts.
    """
    df = pd.read_csv(file_path)
    filtered_df = df[df['Abstract'] != 'No Abstract']
    return filtered_df

def apply_preprocessing(df, text_column):
    """
    Apply the preprocessing function to a specific column in the DataFrame.
    """
    # Using progress_apply instead of apply to have a progress bar
    tqdm.pandas(desc="Processing")  # Initialize tqdm with pandas support
    df[text_column] = df[text_column].progress_apply(preprocess_text)
    return df

def save_data(df, text_column, file_path):
    """
    Save the preprocessed text to a CSV file.
    """
    df[text_column].to_csv(file_path, index=False)

if __name__ == "__main__":
    print("Loading and filtering data...")
    filtered_df = load_and_filter_data('result_samples//pubmed_disease_studies.csv')

    print("Preprocessing text data...")
    processed_df = apply_preprocessing(filtered_df, 'Abstract')

    print("Saving processed data...")
    save_data(processed_df, 'Abstract', 'result_samples//pubmed_disease_studies_filtered.csv')
    print("Completed! Processed data saved.")

