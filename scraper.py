import argparse
import pandas as pd
import math
from Bio import Entrez

def search(query):
    """
    Search in PubMed with a specified query.
    """
    Entrez.email = 'manaal.ahikki@gmail.com'
    handle = Entrez.esearch(db='pubmed', sort='relevance', retmax='250000', retmode='xml', term=query)
    results = Entrez.read(handle)
    return results

def fetch_details(id_list):
    """
    Fetch details for a list of IDs.
    """
    ids = ','.join(id_list)
    Entrez.email = 'manaal.ahikki@gmail.com'
    handle = Entrez.efetch(db='pubmed', retmode='xml', id=ids)
    results = Entrez.read(handle)
    return results

def parse_arguments():
    """
    Parse command-line arguments.
    """
    parser = argparse.ArgumentParser(description='PubMed Scraper')
    parser.add_argument('--chunk_size', type=int, default=100, help='Chunk size for PubMed fetching')
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_arguments()
    chunk_size = args.chunk_size
    print(f'Running script with chunk size: {chunk_size}')

    print('Searching for studies on Disease...')
    studies = search('Disease')
    studiesIdList = studies['IdList']

    title_list = []
    abstract_list = []
    journal_list = []
    language_list = []
    pubdate_year_list = []
    pubdate_month_list = []

    print('Fetching study details...')
    total_chunks = math.ceil(len(studiesIdList) / chunk_size)  # We use math.ceil to round up
    for chunk_i in range(0, len(studiesIdList), chunk_size):
        chunk = studiesIdList[chunk_i:chunk_i + chunk_size]
        papers = fetch_details(chunk)
        print(f'Processing chunk {chunk_i // chunk_size + 1}/{total_chunks}')

        for i, paper in enumerate(papers['PubmedArticle']):
            title_list.append(paper['MedlineCitation']['Article']['ArticleTitle'])

            try:
                abstract_list.append(paper['MedlineCitation']['Article']['Abstract']['AbstractText'][0])
            except:
                abstract_list.append('No Abstract')

            journal_list.append(paper['MedlineCitation']['Article']['Journal']['Title'])
            language_list.append(paper['MedlineCitation']['Article']['Language'][0])

            try:
                pubdate_year_list.append(paper['MedlineCitation']['Article']['Journal']['JournalIssue']['PubDate']['Year'])
            except:
                pubdate_year_list.append('No Data')

            try:
                pubdate_month_list.append(paper['MedlineCitation']['Article']['Journal']['JournalIssue']['PubDate']['Month'])
            except:
                pubdate_month_list.append('No Data')

    print('Creating DataFrame...')
    df = pd.DataFrame(list(zip(title_list, abstract_list, journal_list, language_list, pubdate_year_list, pubdate_month_list)),
                      columns=['Title', 'Abstract', 'Journal', 'Language', 'Year', 'Month'])

    print('Saving DataFrame to CSV...')
    df.to_csv('pubmed_disease_studies.csv', index=False)

    print('Completed! CSV file saved.')
