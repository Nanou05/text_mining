# Importation des bibliothèques nécessaires
from transformers import MarianMTModel, MarianTokenizer
import pandas as pd
from tqdm.notebook import tqdm
import warnings
warnings.filterwarnings("ignore")


# Nombre de lignes à traduire
num_rows = 10  # Remplacez None par le nombre de lignes que vous voulez traduire, ou laissez-le à None pour traduire toutes les lignes.

def load_data(file_path):
    """
    Chargez les données à partir d'un fichier CSV.
    """
    df = pd.read_csv(file_path)
    return df

def translate(text, src_lang, trg_lang):
    """
    Traduire le texte de src_lang à trg_lang en utilisant le modèle MarianMT.
    """
    model_name = f'Helsinki-NLP/opus-mt-{src_lang}-{trg_lang}'
    tokenizer = MarianTokenizer.from_pretrained(model_name)
    model = MarianMTModel.from_pretrained(model_name)

    tokenized_text = tokenizer(text, return_tensors='pt', truncation=True, max_length=800, padding='longest')

    model.config.max_length = tokenized_text['input_ids'].shape[-1]
    translation = model.generate(**tokenized_text)

    translated_text = tokenizer.batch_decode(translation, skip_special_tokens=True)

    return translated_text[0]


def apply_translation(df, text_column, src_lang, trg_lang, new_column_name, nrows):
    """
    Appliquer la fonction de traduction à une colonne spécifique dans le DataFrame.
    """
    tqdm.pandas(desc=f"Translating from {src_lang} to {trg_lang}") 
    df.loc[:nrows, new_column_name] = df.loc[:nrows, text_column].progress_apply(lambda x: translate(x, src_lang, trg_lang))
    return df

def save_data(df, file_path):
    """
    Enregistrer le DataFrame dans un fichier CSV.
    """
    df.to_csv(file_path, index=False)

# Chargez les données
print("Chargement des données...")
filtered_df = load_data('result_samples//pubmed_disease_studies_filtered.csv')  # Mettez le chemin de votre fichier ici

# Calculez le nombre de lignes à traduire
nrows = filtered_df.shape[0] - 1 if num_rows is None else num_rows - 1

print("Traduction des résumés en français...")
translated_df_fr = apply_translation(filtered_df, 'Abstract', 'en', 'fr', 'translated_abstract_fr', nrows)

print("Traduction des résumés en espagnol...")
translated_df_es = apply_translation(translated_df_fr, 'Abstract', 'en', 'es', 'translated_abstract_es', nrows)

print("Enregistrement des données traduites...")
save_data(translated_df_es, 'result_samples//pubmed_disease_studies_translated.csv')  # Mettez le chemin où vous voulez enregistrer le fichier ici
print("Terminé ! Les données traduites ont été enregistrées.")

