

from transformers import AutoModelForSeq2SeqLM, AutoTokenizer
import pandas as pd
from tqdm.notebook import tqdm
import warnings

warnings.filterwarnings("ignore")

# Number of rows to translate
num_rows = 10  # Replace None with the number of rows you want to translate, or leave it as None to translate all rows.

def load_data(file_path):
    """
    Load data from a CSV file.
    """
    df = pd.read_csv(file_path)
    return df

def translate(text, src_lang, trg_lang):
    """
    Translate text from src_lang to trg_lang using the BERT model.
    """
    model_name = f'Helsinki-NLP/opus-mt-{src_lang}-{trg_lang}'
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    model = AutoModelForSeq2SeqLM.from_pretrained(model_name)

    inputs = tokenizer.encode(text, return_tensors="pt")
    outputs = model.generate(inputs, max_length=512, num_beams=4, early_stopping=True)
    translated_text = tokenizer.decode(outputs[0], skip_special_tokens=True)

    return translated_text



def apply_translation(df, text_column, src_lang, trg_lang, new_column_name, nrows):
    """
    Apply the translation function to a specific column in the DataFrame.
    """
    tqdm.pandas(desc=f"Translating from {src_lang} to {trg_lang}")
    df.loc[:nrows, new_column_name] = df.loc[:nrows, text_column].progress_apply(lambda x: translate(x, src_lang, trg_lang))
    return df

def save_data(df, file_path):
    """
    Save the DataFrame to a CSV file.
    """
    df.to_csv(file_path, index=False)

# Load data
print("Loading data...")
filtered_df = load_data('result_samples//pubmed_disease_studies_filtered.csv')  # Put your file path here

# Calculate the number of rows to translate
nrows = filtered_df.shape[0] - 1 if num_rows is None else num_rows - 1

print("Translating abstracts to French...")
translated_df_fr = apply_translation(filtered_df, 'Abstract', 'en', 'fr', 'translated_abstract_fr', nrows)

print("Translating abstracts to Spanish...")
translated_df_es = apply_translation(translated_df_fr, 'Abstract', 'en', 'es', 'translated_abstract_es', nrows)

print("Saving translated data...")
save_data(translated_df_es, 'result_samples//pubmed_disease_studies_translated_bert.csv')  # Put the file path where you want to save the file
print("Done! Translated data has been saved.")
